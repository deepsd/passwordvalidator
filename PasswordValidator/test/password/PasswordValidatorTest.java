/*
 * @author Deep Shah, 023069
 */
package password;
import static org.junit.Assert.*;

import org.junit.Test;


public class PasswordValidatorTest {

	@Test
	public void testHasValidCaseCharsRegular() {
		boolean result = PasswordValidator.hasValidCaseChars("aaaAaaaAaa");
		assertTrue("Invalid case characters", result);
	}
	
	
	@Test
	public void testHasValidCaseCharsExceptionBoundaryIn() {
		boolean result = PasswordValidator.hasValidCaseChars("aA");
		assertTrue("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		boolean result = PasswordValidator.hasValidCaseChars("");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		boolean result = PasswordValidator.hasValidCaseChars(null);
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNumbers() {
		boolean result = PasswordValidator.hasValidCaseChars("12345");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBoundaryOutUpper() {
		boolean result = PasswordValidator.hasValidCaseChars("AAAA");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBoundaryOutLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aaaa");
		assertFalse("Invalid case characters", result);
	}

	
	@Test
	public void testIsValidLengthRegular() {
		assertTrue ("Invalid Password Length", PasswordValidator.isValidLength("1234567890"));
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse ("Invalid Password Length", PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid Password Length", PasswordValidator.isValidLength("12345678"));
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse ("Invalid Password Length", PasswordValidator.isValidLength("1234567"));
	}
	
	@Test
	public void testHasTwoDigitsRegular() {
		assertTrue("Invalid Password", PasswordValidator.hasTwoDigits("testing1234"));
	}
	
	@Test
	public void testHasTwoDigitsException() {
		assertFalse("Invalid Password", PasswordValidator.hasTwoDigits(null));
	}
	
	@Test
	public void testHasTwoDigitsBoundaryIn() {
		assertTrue("Invalid Password", PasswordValidator.hasTwoDigits("testing12"));
	}

	@Test
	public void testHasTwoDigitsBoundaryOut() {
		assertFalse("Invalid Password", PasswordValidator.hasTwoDigits("testing1"));
	}
	
}
