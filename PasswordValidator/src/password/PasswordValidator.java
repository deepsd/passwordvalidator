package password;

/*
 * @author Deep Shah, 023069
 * This class validates passwords and it will be developed using TDD.
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;

	
	/**
	 * 
	 * @param Password
	 * @return
	 */
	public static boolean hasValidCaseChars(String password) {
		return password!=null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*") ;
	}
	
	/**
	 * validates length of password
	 * @param password
	 * @return
	 */
	
	public static boolean isValidLength(String password) {

		//added null check for exception case
		if(password!=null) {
			return password.trim().length() >= MIN_LENGTH;
		}else {
			return false;
		}
	}
	
	/**
	 * validates if password has at least has two digits
	 * @param password
	 * @return
	 */
	
	public static boolean hasTwoDigits(String password) {
		//first checking if length is correct
		   boolean isValid = PasswordValidator.isValidLength(password);
		   if(isValid) { //if valid then check if it has two digits
			   int digits = 0; 
			   //added null check for exception case
			   if(password!=null) {
			       for (int i = 0; i < password.length(); i++) { 
				         if (password.charAt(i) >= 48 && password.charAt(i) <= 57) {
				             digits++; 
				          }
				       }
				       return digits >= MIN_DIGITS; 
			   }else {
				   return false;
			   }
		   }else { 
			   return false;
		   }
	}
	
	
}
